import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'FitConnect-Demo',
  webDir: 'www',
  server: {
    androidScheme: 'ionic',
    hostname: 'io.ionic.starter',
  },
};

export default config;
